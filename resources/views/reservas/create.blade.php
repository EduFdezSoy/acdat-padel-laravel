@extends('layouts.app')

@section('content')
<style>
    .uper {
        margin-top: 40px;
    }
</style>
<div class="container">
    <div class="card uper">
        <div class="card-header">
            Add Site
        </div>
        <div class="card-body">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
            @endif
            <!-- TODO: cambiar formato por un selector de horas entre las disponibles y limitar a max 2 horas -->
            <form method="post" action="{{ route('reservas.store') }}">
                <div class="form-group">
                    @csrf
                    <label for="start">Reservar el dia:</label>
                    <input type="date" class="form-control" name="date"/>
                </div>
                <div class="form-group">
                    <label for="end">Desde las:</label>
                    <input type="time" class="form-control" name="start"/>
                </div>
                <div class="form-group">
                    <label for="end">Hasta las:</label>
                    <input type="time" class="form-control" name="end"/>
                </div>
                <button type="submit" class="btn btn-primary">Reservar</button>
                <a class="btn btn-secondary" href="{{ route('reservas.index') }}"> Cancel</a>
            </form>
        </div>
    </div>
</div>
@endsection