@extends('layouts.app')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="container">
  <div class="card uper">
    <div class="card-header">
      Editar Reserva
    </div>
    <div class="card-body">
      @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div><br />
      @endif
      <form method="post" action="{{ route('reservas.update', $reserva->id) }}">
        @method('PUT')
        @csrf
        <div class="form-group">
          <label for="start">Desde:</label>
          <input type="text" class="form-control" name="start" value={{ $reserva->start }} />
          <!-- TODO: castear la mierda que llega a lo mismo que el create (hacer primero create) -->
        </div>
        <div class="form-group">
          <label for="end">Hasta:</label>
          <input type="text" class="form-control" name="end" value={{ $reserva->end }} />
        </div>
        <button type="submit" class="btn btn-primary">Actualizar</button>
        <a class="btn btn-secondary" href="{{ route('reservas.index') }}"> Cancel</a>
      </form>
    </div>
  </div>
</div>
@endsection