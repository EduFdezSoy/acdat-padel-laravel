@extends('layouts.app')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>

<div class="container">
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="row">

        <div class="pull-left col-8">
          <h2>Listado de reservas de la pista de padel</h2>
        </div>
        <div class="pull-right col-4" align="right">
          <a class="btn btn-success mr-4 mb-2" href="{{ route('reservas.create') }}"> Reservar pista</a>
        </div>
      </div>
    </div>
  </div>

  <div class="uper">
    @if (session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}
    </div><br />
    @endif
    @if (session()->get('error'))
    <div class="alert alert-danger">
      {{ session()->get('error') }}
    </div><br />
    @endif

    <table class="table table-striped">
      <thead>
        <tr>
          <td>ID</td>
          <td>Desde</td>
          <td>Hasta</td>
          <td>Usuario</td>
          <td colspan="2">Action</td>
        </tr>
      </thead>
      <tbody>
        @foreach($reservas as $reserva)
        <tr>
          <td>{{$reserva->id}}</td>
          <td>{{$reserva->start}}</td>
          <td>{{$reserva->end}}</td>
          <td>{{$reserva->user_id}}</td>
          <td><a href="{{ route('reservas.edit',$reserva->id)}}" class="btn btn-primary">Editar</a></td>
          <td>
            <form action="{{ route('reservas.destroy', $reserva->id)}}" method="post">
              @csrf
              @method('DELETE')
              <button class="btn btn-danger" type="submit">Borrar</button>
            </form>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    <div>
    </div>
    @endsection