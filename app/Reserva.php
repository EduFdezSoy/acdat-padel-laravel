<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reserva extends Model
{
    protected $fillable = ['start', 'end', 'user_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
