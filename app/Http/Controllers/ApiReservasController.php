<?php

namespace App\Http\Controllers;

use App\Reserva;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ApiReservasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $reservas = Reserva::get();

        $lim = sizeof($reservas);
        for ($i = 0; $i < $lim; $i++) {
            $reservas[$i]['user_name'] = User::find($reservas[$i]['user_id'])->name;
        }

        return response()->json($reservas, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // TODO: comprobar start y end (max dos horas)
        // TODO: comprobar que el usuario no tiene reservas ya para ese dia (solo una por dia)
        // TODO: comprobar que no hay reservas de otro usuario en ese tramo (solo tenemos una pista, no pueden jugar dos a la vez)
        // TODO: comprobar que la reserva no es de mas de una semana en el futuro
        // TODO: comprobar que la reserva no se hace en el pasado

        // creo que este if puede ser util para comprobar esas cosas:
        // if ($reserva['user_id'] != Auth::id()) {
        //     $status = "error";
        //     $message = "No se puede editar la reserva de otro usuario";
        //     return redirect('/reservas')->with($status, $message);
        // }

        $validator = Validator::make($request->all(), [
            'start' => 'required',
            'end' => 'required',
        ]);

        if ($validator->fails()) {
            $message = $validator->errors()->getMessages();
            $status = 422;
            //return response()->json( $validator->errors()->getMessages(), 422);
        } else {
            $start = $request->get('start');
            $end = $request->get('end');

            $reserva = new Reserva([
                'start' => $start,
                'end' => $end,
                'user_id' => Auth::id(),
            ]);

            if ($reserva->save()) {
                //return response()->json( $site, 201);
                $message = $reserva;
                $status = 201;
            } else {
                //return response()->json( 'El sitio no pudo ser añadido', 500);
                $message = 'No se pudo reservar, algo salio mal';
                $status = 500;
            }
        }

        return response()->json($message, $status);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $reserva = Reserva::find($id);

        if (!$reserva) {
            $message = 'Reserva no encontrado';
            $status = 404;
        } else {
            $message['reserva'] = $reserva;
            $status = 200;
        }

        return response()->json($message, $status);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reserva $reserva)
    {
        $reserva = auth()->user()->reservas()->find($reserva->id);

        if (!$reserva) {
            $message = 'Reserva no encontrada';
            $status = 404;
        } else {
            // TODO: aqui hay que validarlo todo tambien
            $validator = Validator::make($request->all(), [
                'start' => 'required',
                'end' => 'required',
            ]);

            if ($validator->fails()) {
                $message = $validator->errors()->getMessages();
                $status = 422;
                //return response()->json( $validator->errors()->getMessages(), 422);
            } else {

                $updated = $reserva->update(array(
                    'start' => $request->get('start'),
                    'end' => $request->get('end'),
                ));
                //$updated = $site->fill($request->all())->save();

                if ($updated) {
                    $message = $reserva;
                    $status = 201;
                } else {
                    $message = 'La reserva ' . $reserva . ' no se pudo actualizar';
                    $status = 500;
                }
            }
        }

        return response()->json($message, $status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reserva $reserva)
    {
        $reserva = auth()->user()->reservas()->find($reserva->id);

        if (!$reserva) {
            $message = 'Reserva no encontrada, quizas no es tuya';
            $status = 404;
        } else {
            if ($reserva->delete()) {
                $message = null;
                $status = 204;
            } else {
                $message = 'La reserva no pudo ser eliminada';
                $status = 500;
            }
        }

        return response()->json($message, $status);
    }
}
