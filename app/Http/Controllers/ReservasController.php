<?php

namespace App\Http\Controllers;

use App\Reserva;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReservasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reservas = Reserva::get();

        $lim = sizeof($reservas);
        for ($i=0; $i < $lim; $i++) { 
            $reservas[$i]['user_id'] = User::find($reservas[$i]['user_id'])->name;
        }

        return view('reservas.index', compact('reservas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('reservas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'date' => 'required|date',
            'start' => 'required',
            'end' => 'required',
        ]);

        // TODO: comprobar start y end (max dos horas)
        // TODO: comprobar que el usuario no tiene reservas ya para ese dia (solo una por dia)
        // TODO: comprobar que no hay reservas de otro usuario en ese tramo (solo tenemos una pista, no pueden jugar dos a la vez)
        // TODO: comprobar que la reserva no es de mas de una semana en el futuro
        // TODO: comprobar que la reserva no se hace en el pasado

        // creo que este if puede ser util para comprobar esas cosas:
        // if ($reserva['user_id'] != Auth::id()) {
        //     $status = "error";
        //     $message = "No se puede editar la reserva de otro usuario";
        //     return redirect('/reservas')->with($status, $message);
        // }

        $date = $request->get('date');
        $start = $date . " " . $request->get('start');
        $end = $date . " " . $request->get('end');

        $reserva = new Reserva([
            'start' => $start,
            'end' => $end,
            'user_id' => Auth::id(),
        ]);
        $reserva->save();

        $status = "success";
        $message = "La reserva " . $reserva . " se ha anadido";

        return redirect('/reservas')->with($status, $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reserva = Reserva::findOrFail($id);

        if ($reserva['user_id'] != Auth::id()) {
            $status = "error";
            $message = "No se puede editar la reserva de otro usuario";
            return redirect('/reservas')->with($status, $message);
        }

        return view('reservas.edit', compact('reserva'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // TODO: aqui hay que validarlo todo tambien
        $request->validate([
            'start' => 'required|date',
            'end' => 'required|date',
        ]);

        $reserva = Reserva::findOrFail($id);
        $reserva->start = $request->get('start');
        $reserva->end = $request->get('end');
        $reserva->save();

        $status = "success";
        $message = "La reserva " . $reserva . " se ha actualizado";
        return redirect('/reservas')->with($status, $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reserva = Reserva::find($id);

        if ($reserva['user_id'] != Auth::id()) {
            $status = "error";
            $message = "No se puede borrar la reserva de otro usuario";
            return redirect('/reservas')->with($status, $message);
        }

        $reserva->delete();
        $status = "success";
        $message = "La reserva " . $reserva . " se ha borrado";
        return redirect('/reservas')->with($status, $message);
    }
}